import { combineReducers } from 'redux';
import { wishReducer} from '../modules/grantWish';

const makeRootReducer = combineReducers({
  wish: wishReducer 
});

export default makeRootReducer;
