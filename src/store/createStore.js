import { applyMiddleware, compose, createStore } from 'redux';
import { apiMiddleware } from 'redux-api-middleware';
import thunk from 'redux-thunk';
import makeRootReducer from './reducers';

export function configureStore(initialState) {
    const middlewares = [
        apiMiddleware,
        thunk
    ];

    const composeEnhancers = (
        typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

    let newState = initialState || {};

    const store = createStore(makeRootReducer, newState, composeEnhancers(
        applyMiddleware(...middlewares)
    ));

    return store;
}
