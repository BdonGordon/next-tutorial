export const ADD_TO_WISHLIST_REQUEST = 'about/ADD_TO_WISHLIST_REQUEST';
export const ADD_TO_WISHLIST_RESPONSE = 'about/ADD_TO_WISHLIST_RESPONSE';
export const ADD_TO_WISHLIST_ERROR = 'about/ADD_TO_WISHLIST_ERROR';

const initialState = {
  isFetching: false,
  wish: null
};

export function addWish(wish) {
   return {
       type: ADD_TO_WISHLIST_RESPONSE,
       payload: {
          wish: wish
       }
   };
}

export function wishReducer(state = initialState, action) {
  switch(action.type) {
    case ADD_TO_WISHLIST_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true
      });
    }

    case ADD_TO_WISHLIST_RESPONSE: {
      return Object.assign({}, state, {
        isFetching: false,
        wish: action.payload.wish
      });
    }

    case ADD_TO_WISHLIST_ERROR: {
      return Object.assign({}, state, {
        isFetching: false
      });
    }

    default: {
      return state;
    }
  }
}
