import Link from 'next/link';
import React from 'react';

const linkStyle = { marginRight: 15 };

class AppHeader extends React.Component {
  render() {
    return (
      <div>
        <Link href="/">
          <a style={linkStyle}>Home</a>
        </Link>
        <Link href="/about">
          <a style={linkStyle}>About Us</a>
        </Link>
      </div>
    );
  }
}

export default AppHeader;
