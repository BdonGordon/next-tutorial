import React from 'react';
import AppHeader from '../components/AppHeader';
import Head from 'next/head';

const layoutStyle = {
  margin: 20,
  padding: 20
};

class CoreLayout extends React.Component {
  constructor(props) {
    super(props);
  }

  _initializeStyles() {
    return (
      <Head>
        <title>My styles pages</title>
        {/* <link href="/static/core.css" rel="stylesheet" />
        <link href="/static/bootstrap.min.css" rel="stylesheet" />
        <link href="/static/components.css" rel="stylesheet" />
        <link href="/static/colors.min.css" rel="stylesheet" />
        <script src="https://unpkg.com/react/umd/react.production.js" crossorigin />
        <script src="https://unpkg.com/react-dom/umd/react-dom.production.js" crossorigin />
        <script src="https://unpkg.com/react-bootstrap@next/dist/react-bootstrap.min.js" crossorigin /> */}
        <link href="/static/components.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous" 
        />
      </Head>
    );
  }

  render() {
    return (
      <div style={layoutStyle}>
        {this._initializeStyles()}
        <AppHeader />
        {this.props.children}
      </div>
    );
  }
}

export default CoreLayout;
