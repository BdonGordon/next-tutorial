import React from 'react';
import Link from 'next/link';
import { Button, Row, Col } from 'react-bootstrap';

class Index extends React.Component {
  constructor(props) {
    super(props);
  }

  _handleButtonClick() {
    alert("Hello");
  }

  render() {
    const { shows } = this.props;
    return (
      <div>
        <ul>
          {shows.map(show => (
            <li key={show.id}>
              <Link as={`/posts/${show.id}`} href={`/post?title=${show.id}`}>
                <a>{show.name}</a>
              </Link>
            </li>
          ))}
        </ul>
        
        <Row>
          <Col sm={6}>
            <Button variant="info" onClick={this._handleButtonClick}>Hello</Button>
          </Col>
          <Col sm={6}>
            <Button variant="outline-success" onClick={this._handleButtonClick}>Hello</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Index;
