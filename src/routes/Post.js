import React from 'react';
import { withRouter } from 'next/router';

class Post extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <h1>{this.props.router.query.title}</h1>
        <p>This is the blog post content.</p>
      </div>
    );
  }
}

export default withRouter(Post);
