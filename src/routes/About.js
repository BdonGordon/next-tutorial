import React from 'react';
import { Card, Form, Row, Col, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { addWish } from '../modules/grantWish';

class About extends React.Component {
  constructor(props) {
    super(props);

    this.state = { 
      wish: '',
      name: ''
    };
    this.handleWishChange = this.handleWishChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleWishChange(e) {
    this.setState({ wish: e.currentTarget.value });
  }

  handleNameChange(e) {
    this.setState({ name: e.currentTarget.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    const wish = { wish: this.state.wish, name: this.state.name };
    this.props.addWish(wish);
  }

  render() {
    return (
      <Card border="dark">
        <Card.Header>About Us</Card.Header>
        <Card.Body>
          <Card.Title>Dark Card Title</Card.Title>
          <Card.Text>
            <Form onSubmit={this.handleSubmit}>
              <Row>
                <Col md={4}>
                  <Form.Group>
                    <Form.Label>Tell Shenron Wagwan</Form.Label>
                    <Form.Control type="text" placeholder="Enter your wish" onChange={this.handleWishChange}/>
                    <Form.Text className="text-muted">You gotta have the 7 Dragon Balls styll</Form.Text>
                  </Form.Group>
                </Col>
                <Col md={4}>
                  <Form.Group>
                    <Form.Label>Saiyan Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter your name" onChange={this.handleNameChange}/>
                  </Form.Group>
                </Col>
              </Row>

              <Row>
                <Button className="ml-10" type="submit">Submit</Button>
              </Row>
            </Form>
          </Card.Text>
        </Card.Body>
      </Card>
    );
  }
}

//TODO: Throw in Container file
function mapStateToProps(state) {
  return {
    isFetching: state.wish.isFetching
  };
}

function mapDispatchToProps(dispatch) {
  return {
      addWish: (wish) => dispatch(addWish(wish))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(About);
