const withCSS = require('@zeit/next-css')
module.exports = withCSS({ cssModules: true })

/* With additional configuration on top of CSS Modules */
module.exports = withCSS({
  cssModules: true,
  webpack: function (config) {
    return config;
  }
});