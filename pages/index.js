import React from 'react';
import CoreLayout from '../src/layouts/CoreLayout';
import Index from '../src/routes/Index';
import fetch from 'isomorphic-unfetch';
import { configureStore } from '../src/store/createStore';
const store = configureStore();

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
  }

  static async getInitialProps() {
    const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
    const data = await res.json();
    // Use if no interet: const data = [{show: {id: 'id1', name: 'Name1'}}, {show:{id: 'id2', name: 'Name2'}}];

    return {
      shows: data.map(entry => entry.show)
    };
  }

  render() {
    return (
      <CoreLayout>
        <Index shows={this.props.shows} store={store}/>
      </CoreLayout>
    );
  }
}

export default IndexPage;
