import React from 'react';
import CoreLayout from '../src/layouts/CoreLayout';
import Post from '../src/routes/Post';

class PostPage extends React.Component{
  render() {
    return (
      <CoreLayout>
        <Post />
      </CoreLayout>
    );
  }
}

export default PostPage;
