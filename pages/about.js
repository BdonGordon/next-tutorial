import React from 'react';
import CoreLayout from '../src/layouts/CoreLayout';
import About from '../src/routes/About';
import { configureStore } from '../src/store/createStore';
const store = configureStore();

class AboutPage extends React.Component {
  render() {
    return (
      <CoreLayout>
        <About store={store}/>
      </CoreLayout>
    );
  }
}

export default AboutPage;
